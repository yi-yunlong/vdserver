package com.server.controller;

import com.alibaba.fastjson2.JSONObject;
import com.server.domain.param.VideoDownloadParam;
import com.server.domain.vo.VideoResourceVo;
import com.server.service.VideoDownloadService;
import com.server.thread.VideoDownloadThread;
import com.server.utils.VideoResourceUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * Author: thelongi
 * Date: 2022/6/29
 * Describe: 视频控制器
 */

@RestController
@RequestMapping("vedio")
@Log4j2
public class VideoDownloadController {

    @Autowired
    VideoDownloadService videoDownloadService;


    @PostMapping("download")
    //视频下载
    public JSONObject vedioDownload(@Valid @RequestBody VideoDownloadParam param){
        log.info("开始下载 param:"+param);
        //定义返回对象
        JSONObject jsonObject = new JSONObject();
        //rpc调用接口 拿到 视频下载资源
        List list = videoDownloadService.rpcQueryReplay(param);
        if(list == null){
            log.info("rpc调用失败 data为null");
            jsonObject.put("code",501);
            jsonObject.put("type","warning");
            jsonObject.put("message","rpc调用失败 data为null");
            return jsonObject;
        }
        //清理数据资源
        List<VideoResourceVo> cleanedList = VideoResourceUtil.cleanData(list);
        //给线程分配资源
        List<VideoDownloadThread> threadList = videoDownloadService.distrResource(param, cleanedList);
        //开启线程
        for(VideoDownloadThread thread : threadList){
            thread.start();
        }
        //等待所有线程下载完毕
        waitAllThreadFinish(threadList);
        //todo 合成为一个视频

        log.info("下载结束");
        //定义返回对象
        jsonObject.put("code",200);
        jsonObject.put("type","success");
        jsonObject.put("message","下载成功");
        return jsonObject;
    }

    private void waitAllThreadFinish(List<VideoDownloadThread> threadList) {
        //死循环等待所有线程下载完毕 结束
        while (true){
            // true表示 所有下载完毕
            boolean flag = true;
            for(VideoDownloadThread thread : threadList){
                //线程没下完
                if (!thread.getFinish()) {
                    flag = false;
                    break;
                }
            }
            if(flag) break;
        }
    }
}
