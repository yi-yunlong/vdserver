package com.server.thread;

import com.server.domain.vo.VideoResourceVo;
import com.server.utils.VideoDownloadUtils;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * Author: thelongi
 * Date: 2022/6/29
 * Describe:
 */

@Log4j2
public class VideoDownloadThread extends Thread {

    //下载的目标文件夹
    private String descAbsPath;
    //视频名称
    private String videoName;
    //待下载的资源
    private List<VideoResourceVo> resourceList;
    //下载结束标识
    private Boolean isFinish = false;

    @SneakyThrows
    @Override
    public void run() {
        String name = Thread.currentThread().getName();
        log.info(name+"准备下载==============>"+resourceList.size()+"条数据"+" 目标文件夹:"+descAbsPath);
        //下载视频
        VideoDownloadUtils.downloadResourceList(resourceList,descAbsPath,videoName);
        //下载完毕
        this.isFinish = true;
        log.info(name+"下载完毕 共下载==============>"+resourceList.size()+"条数据");

    }

    public String getDescAbsPath() {
        return descAbsPath;
    }

    public void setDescAbsPath(String descAbsPath) {
        this.descAbsPath = descAbsPath;
    }

    public String getVideoName() {
        return videoName;
    }

    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }

    public List<VideoResourceVo> getResourceList() {
        return resourceList;
    }

    public void setResourceList(List<VideoResourceVo> resourceList) {
        this.resourceList = resourceList;
    }

    public Boolean getFinish() {
        return isFinish;
    }

    public void setFinish(Boolean finish) {
        isFinish = finish;
    }

    public static Logger getLog() {
        return log;
    }
}
