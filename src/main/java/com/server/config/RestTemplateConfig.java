package com.server.config;


import lombok.Data;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * Author: thelongi
 * Date: 2022/6/9
 * Describe:
 */

@Data
@Configuration
public class RestTemplateConfig {
    //连接超时时间
    private Integer connectionTimeout = 1000*60;
    //读取数据超时时间
    private Integer readTimeout = 1000*60;


    @Bean
    public RestTemplate restTemplate(ClientHttpRequestFactory factory) {
        return new RestTemplate(factory);
    }

    @Bean
    public ClientHttpRequestFactory simpleClientHttpRequestFactory() {
        SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        //建⽴连接所⽤的时间，适⽤于⽹络状况正常的情况下，两端连接所⽤的时间。单位毫秒
        factory.setConnectTimeout(connectionTimeout);
        //建⽴连接后从服务器读取到可⽤资源所⽤的时间。单位毫秒
        factory.setReadTimeout(readTimeout);
        return factory;
    }

}
