package com.server.domain.vo;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Author: thelongi
 * Date: 2022/6/29
 * Describe:
 */
@Data
@NoArgsConstructor
@ToString
public class VideoResourceVo {
    String startTime;
    String endTime;
    String filePath;
    String duration;
    String updateTime;
}
