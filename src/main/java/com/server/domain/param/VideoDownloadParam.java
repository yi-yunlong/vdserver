package com.server.domain.param;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * Author: thelongi
 * Date: 2022/6/29
 * Describe:视频下载请求参数
 */
@Data
@NoArgsConstructor
@ToString
public class VideoDownloadParam {

    String cameraName;
    String domainNum;
    String serialNumber;
    @NotBlank(message = "下载URL不能为空")
    String downLoadURL;
    String startDate;
    String endDate;
    Boolean isAutoConfig;
    Integer threadNum;
    @NotBlank(message = "下载路径不能为空")
    String dir;
    String videoName;

}
