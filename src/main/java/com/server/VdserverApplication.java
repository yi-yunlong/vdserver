package com.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VdserverApplication {

    public static void main(String[] args) {
        SpringApplication.run(VdserverApplication.class, args);
    }

}
