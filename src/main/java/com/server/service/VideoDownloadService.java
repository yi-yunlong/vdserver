package com.server.service;

import com.alibaba.fastjson2.JSONObject;
import com.server.domain.param.VideoDownloadParam;
import com.server.domain.vo.VideoResourceVo;
import com.server.thread.VideoDownloadThread;
import org.springframework.http.HttpEntity;

import java.util.List;

public interface VideoDownloadService {

    //配置下载视频的Http实体
    public HttpEntity<JSONObject> confiDownloadParam(VideoDownloadParam param);

    //RPC调用接口 查询视频下载资源
    public List rpcQueryReplay(VideoDownloadParam param);

    //给线程合理分配下载资源
    public List<VideoDownloadThread> distrResource(VideoDownloadParam param, List<VideoResourceVo> resourceList);

    //在分片文件夹下将所有视频文件合并成一个mp4文件
    public JSONObject mergeToMP4(VideoDownloadParam param);

}
