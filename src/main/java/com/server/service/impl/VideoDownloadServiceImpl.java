package com.server.service.impl;

import com.alibaba.fastjson2.JSONObject;
import com.server.domain.param.VideoDownloadParam;
import com.server.domain.vo.VideoResourceVo;
import com.server.service.VideoDownloadService;
import com.server.thread.VideoDownloadThread;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Author: thelongi
 * Date: 2022/6/29
 * Describe:
 */
@Service
@Log4j2
public class VideoDownloadServiceImpl implements VideoDownloadService {

    //restTemplate
    @Autowired
    RestTemplate restTemplate;

    @Override
    public HttpEntity<JSONObject> confiDownloadParam(VideoDownloadParam param) {
        // 配置参数
        JSONObject requestMap = new JSONObject();
        requestMap.put("cameraName", param.getCameraName());
        requestMap.put("domainNum", param.getDomainNum());
        requestMap.put("serialNumber", param.getSerialNumber());
        requestMap.put("startTime", param.getStartDate());
        requestMap.put("endTime", param.getEndDate());
        // 配置headers
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        // 返回实体
        return new HttpEntity<>(requestMap, headers);
    }

    @Override
    public List rpcQueryReplay(VideoDownloadParam param) {
        //请求地址
        String url = param.getDownLoadURL();
        //请求实体
        HttpEntity<JSONObject> entity = this.confiDownloadParam(param);
        try {
            log.info("rpc请求视频资源 url:" + url + "entity:" + entity);
            //Post请求
            ResponseEntity<JSONObject> result = restTemplate.postForEntity(url, entity, JSONObject.class);
            JSONObject body = result.getBody();
            if (result.getStatusCodeValue() != 200 || body == null || (Integer) body.get("code") != 200) {
                log.info("请求失败");
                return null;
            }
            log.info("rpc请求成功 code:" + (Integer) body.get("code"));
            return (ArrayList<Map<String, Object>>) body.get("data");
        } catch (Exception e) {
            //打印错误信息
            log.info("异常错误 错误信息");
            e.printStackTrace();
            //Get不到响应返回空
            return null;
        }
    }

    @Override
    public List<VideoDownloadThread> distrResource(VideoDownloadParam param, List<VideoResourceVo> resourceList) {
        // 线程数
        Integer threadNum;
        //总资源数
        int size = resourceList.size();
        log.info("开始分配共"+size+"条资源");
        //如果是自动配置线程数
        if (param.getIsAutoConfig()) {
            //根据算法分配 线程数
            threadNum = 5;
        } else {
            //拿到线程数
            threadNum = param.getThreadNum();
        }
        //计算每个线程的待下载资源数
        Integer num = size / threadNum;
        log.info("总线程数:"+threadNum);
        log.info("预计每个线程分配资源数:"+num);
        //线程列表
        List<VideoDownloadThread> list = new ArrayList<VideoDownloadThread>();
        // 根据线程数合理分配下载资源
        for (int i = 0; i < threadNum - 1; i++) {
            //定义线程
            VideoDownloadThread thread = new VideoDownloadThread();
            thread.setDescAbsPath(param.getDir());
            thread.setVideoName(param.getVideoName());
            // 1(0,num-1) 2(num,2num-1) 分配资源
            List<VideoResourceVo> videoResourceVos = resourceList.subList(i * num, (i + 1) * num);
            thread.setResourceList(videoResourceVos);
            //添加进线程列表
            list.add(thread);
        }
        //给最后一个线程分配资源
        VideoDownloadThread thread = new VideoDownloadThread();
        thread.setDescAbsPath(param.getDir());
        thread.setVideoName(param.getVideoName());
        // i=0(0,num-1) i=1(num,2num-1) i=2(2num,3num-1) i=threadNum-1(threadNum-1, size-1)
        thread.setResourceList(resourceList.subList((threadNum - 1) * num, size));
        //添加进线程列表
        list.add(thread);
        return list;
    }

    @Override
    public JSONObject mergeToMP4(VideoDownloadParam param) {
        return null;
    }


}
