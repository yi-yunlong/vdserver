package com.server.utils;

import com.server.domain.vo.VideoResourceVo;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import sun.security.util.Length;

import java.io.File;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;


/**
 * Author: thelongi
 * Date: 2022/6/27
 * Describe:
 */

@Log4j2
public class VideoDownloadUtils {

    public static final String MODE = "no-cors";
    public static final String AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36";
    public static final Integer TIME_OUT = 10 * 1000;

    //下载分片资源集合
    public static void downloadResourceList(List<VideoResourceVo> resourceList, String descAbsPath, String videoName) {
        for (VideoResourceVo vo : resourceList) {
            download(vo.getFilePath(), descAbsPath, videoName);
        }
    }

    //下载分片ftv
    public static void download(String urlPath, String descAbsPath, String videoName) {
        //通过URL拿到分片文件名称
        String fileName = getFileName(urlPath);
        //拿到当前线程名称
        String name = Thread.currentThread().getName();
        InputStream inputStream = null;
        try {
            long begin = System.currentTimeMillis();
            URL url = new URL(urlPath);
            URLConnection urlConnection = url.openConnection();
            urlConnection.setRequestProperty("Sec-Fetch-Mode", MODE);
            urlConnection.setRequestProperty("User-Agent", AGENT);
            urlConnection.setConnectTimeout(TIME_OUT);
            log.info(name+"下载"+fileName+" 共:" + (urlConnection.getContentLength() / 1024) + "Kb");
            log.info(name+"开始下载"+fileName);
            InputStream input = urlConnection.getInputStream();
            FileUtils.copyInputStreamToFile(input, new File(descAbsPath + "\\" + videoName + "\\" + fileName));
            long end = System.currentTimeMillis();
            log.info(name+"下载"+fileName+"耗时：" + (end - begin) / 1000 + "秒");
            log.info(name+"下载"+fileName+"完成！");
        } catch (Exception e) {
            log.info(name+"异常中止: " + e);
        }
    }

    private static String getFileName(String urlPath) {
        String[] split = StringUtils.split(urlPath, "/");
        return split.length == 0 ? null : split[split.length - 1];
    }


    public static void main(String[] args) {
        String urlPath = " http://192.168.2.102:81/34020000002000000002@34020000001320000096/20220628/1656400081572.flv";
        download(urlPath, "E:\\CodeRepository\\MyCode\\electron-quicstart\\resources", "sp01");
    }

}
