package com.server.utils;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.server.domain.vo.VideoResourceVo;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Author: thelongi
 * Date: 2022/6/29
 * Describe:
 */
public class VideoResourceUtil {

    //数据清理 只需要filepath存在的对象
    public static List<VideoResourceVo> cleanData(List<Map<String, Object>> list) {
        //清洗数据
        List<Map<String, Object>> collect = list.stream().filter(e -> {
            return e.get("filePath") != null && !e.get("filePath").equals("");
        }).collect(Collectors.toList());
        System.out.println(collect.toString());
        //得到List<VedioResourceVo>
        return collect.stream().map(e -> {
            String jsonObject = JSON.toJSONString(e);
            VideoResourceVo videoResourceVo = JSONObject.parseObject(jsonObject, VideoResourceVo.class);
            return videoResourceVo;
        }).collect(Collectors.toList());
    }

    /*** 遍历某一个路径的所有文件，包括子文件夹
     *@parampath
     *@return
     */
    public static List getAllFilesAndDir(String path) {
        File root = new File(path);
        List files = new ArrayList();
        if (!root.isDirectory()) {
            files.add(root);
        } else {
            File[] subFiles = root.listFiles();
            for (File f : subFiles) {
                files.addAll(getAllFilesAndDir(f.getAbsolutePath()));
            }
        }
        return files;
    }

    /*** 遍历某一个路径的所有文件，不包括子文件夹
     *@parampath
     *@return
     */
    public static List<File> getAllFiles(String path) {
        File root = new File(path);//文件路径是文件夹，获取文件夹下的所有文件
        if(root.getName().contains("DS_Store")) {
            System.out.println(root.getName());
        }
        if (root.isDirectory()) {
            //过滤
            //排除文件名含有".DS_Store"的文件
            return Arrays.asList(root.listFiles()).stream().filter(a -> !a.getName().endsWith(".DS_Store")).collect(Collectors.toList());
        }
        return new ArrayList();
    }

    /***
     *@paramlistFiles 遍历得到的某路径下面所有的文件对象
     *@paramdestFullPath 合并的文件的全路径，包括文件名
     *@return
     */
    public static boolean MergeFiles(List<File> listFiles, String destFullPath) {
        boolean flag = true;
        BufferedOutputStream bos = null;
        BufferedReader br = null;
        try {
            FileInputStream fis;
            //打开与目标文件对象的通道
            FileOutputStream fos = new FileOutputStream(destFullPath);
            byte buffer[] = new byte[1024 * 1024 * 2];//一次读取2M数据，将读取到的数据保存到byte字节数组中
            for (File srcFile : listFiles) {//涉及合并文件，最好限制同名文件(根据需求判断是否需要)
                // 判断合并文件的格式
                if (srcFile.isFile() && !srcFile.getName().endsWith(".DS_Store")) {
                    System.out.println(srcFile.getName() + "是合法文件，开始合并");
                    try {//打开与源文件对象的通道
                        fis = new FileInputStream(srcFile);
                        int len = 0;
                        while ((len = fis.read(buffer)) != -1) {
                            fos.write(buffer, 0, len);//buffer从指定字节数组写入。buffer:数据中的起始偏移量,len:写入的字数。
                        }
                        fis.close();
                    } finally {
                        if (br != null) {
                            br.close();
                        }
                    }
                } else {
                    System.out.println(srcFile.getName() + "文件不满足合并格式！");
                }
            }
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            flag = false;
        } catch (IOException e) {
            e.printStackTrace();
            flag = false;
        } finally {
            try {
                if (bos != null) {
                    bos.close();
                } if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
                flag = false;
            }

        }
        return flag;
    }

    //测试
    public static void main(String[] args) {//定义需要复制文件的路径
        String srcPath = "/Users/srcPath";
        String destName = "mergedVideo.mp4";
        String destFullPath = "/Users/destFileDir" + destName;
        List<File> files = getAllFiles(srcPath);
        Collections.sort(files, new Comparator<File>() {
            @Override
            public int compare(File o1, File o2) {
                if (o1.isDirectory() && o2.isFile())
                    return -1;
                if (o1.isFile() && o2.isDirectory())
                    return 1;
                Integer o1Name =Integer.valueOf(o1.getName());
                Integer o2Name =Integer.valueOf(o2.getName());
                return o1Name.compareTo(o2Name);
            }
        });
        for (File file : files) {
            System.out.println(file.getName());
        }

        boolean flag = MergeFiles(files, destFullPath);
        System.out.println("合并完成的标记：" + flag);

    }

}
