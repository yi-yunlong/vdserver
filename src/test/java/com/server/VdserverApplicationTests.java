package com.server;

import com.alibaba.fastjson2.JSON;
import com.server.domain.param.VideoDownloadParam;
import com.server.domain.vo.VideoResourceVo;
import com.server.service.VideoDownloadService;
import com.server.thread.VideoDownloadThread;
import com.server.utils.Mp4ParserUtils;
import com.server.utils.VideoResourceUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@SpringBootTest
class VdserverApplicationTests {

    @Autowired
    VideoDownloadService videoDownloadService;

    @Test
    void contextLoads() {
        String jsonString = "[{\"startTime\":1656399681214},{\"startTime\":1656399741214},{\"startTime\":1656399741681,\"endTime\":1656399856552,\"filePath\":\" http://192.168.2.102:81/34020000002000000002@34020000001320000096/20220628/1656399741681.flv\",\"duration\":100000,\"updateTime\":1656399856556},{\"startTime\":1656399856277,\"endTime\":1656399972145,\"filePath\":\" http://192.168.2.102:81/34020000002000000002@34020000001320000096/20220628/1656399856277.flv\",\"duration\":100000,\"updateTime\":1656399972145},{\"startTime\":1656399972089,\"endTime\":1656400081630,\"filePath\":\" http://192.168.2.102:81/34020000002000000002@34020000001320000096/20220628/1656399972089.flv\",\"duration\":100000,\"updateTime\":1656400081639},{\"startTime\":1656400081572,\"endTime\":1656400197983,\"filePath\":\" http://192.168.2.102:81/34020000002000000002@34020000001320000096/20220628/1656400081572.flv\",\"duration\":100000,\"updateTime\":1656400197991},{\"startTime\":1656400197918,\"endTime\":1656400310397,\"filePath\":\" http://192.168.2.102:81/34020000002000000002@34020000001320000096/20220628/1656400197918.flv\",\"duration\":100000,\"updateTime\":1656400310397},{\"startTime\":1656400310363,\"endTime\":1656400427877,\"filePath\":\" http://192.168.2.102:81/34020000002000000002@34020000001320000096/20220628/1656400310363.flv\",\"duration\":100000,\"updateTime\":1656400427877},{\"startTime\":1656400427870,\"endTime\":1656400544179,\"filePath\":\" http://192.168.2.102:81/34020000002000000002@34020000001320000096/20220628/1656400427870.flv\",\"duration\":100000,\"updateTime\":1656400544182},{\"startTime\":1656400544118,\"endTime\":1656400693854,\"filePath\":\" http://192.168.2.102:81/34020000002000000002@34020000001320000096/20220628/1656400544118.flv\",\"duration\":100000,\"updateTime\":1656400693854},{\"startTime\":1656400659996,\"endTime\":1656400776582,\"filePath\":\" http://192.168.2.102:81/34020000002000000002@34020000001320000096/20220628/1656400659996.flv\",\"duration\":100000,\"updateTime\":1656400776588}]";
        List<Map<String, Object>> list = JSON.parseObject(jsonString, List.class);
        //模拟数据
        List<VideoResourceVo> videoResourceVos = VideoResourceUtil.cleanData(list);
        VideoDownloadParam param = new VideoDownloadParam();
        param.setCameraName("room_001_camera_001");
        param.setDomainNum("34020000002000000002");
        param.setSerialNumber("34020000001320000096");
        param.setDownLoadURL("http://192.168.2.99:5000/video/v1/replay");
        param.setStartDate("2022-06-15 01:00:00");
        param.setEndDate("2022-06-28 07:18:00");
        param.setIsAutoConfig(true);
        param.setDir("E:\\CodeRepository\\MyCode\\electron-quicstart\\resources");
        param.setVideoName("sp01");
        //多线程下载
        List<VideoDownloadThread> threadList = videoDownloadService.distrResource(param, videoResourceVos);
        for(VideoDownloadThread thread : threadList){
            thread.start();
        }
        try {
            //主线程休眠30 秒 等待线程执行
            Thread.sleep(1000 * 60 *5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    void test(){
        String basePath = "E:\\CodeRepository\\MyCode\\electron-quicstart\\resources\\sp01";
        //分片文件的地址
        List<String> videoList = new ArrayList<>();
        videoList.add(basePath+"\\1656399741681.flv");
        videoList.add(basePath+"\\1656399856277.flv");
        //videoList.add(basePath+"\\1656399972089.flv");
        //videoList.add(basePath+"\\1656400081572.flv");
        //videoList.add(basePath+"\\1656400197918.flv");
        //videoList.add(basePath+"\\1656400310363.flv");
        //videoList.add(basePath+"\\1656400427870.flv");
        //videoList.add(basePath+"\\1656400544118.flv");
        //videoList.add(basePath+"\\1656400659996.flv");
        //生成的目标文件
        File file = new File("E:\\CodeRepository\\MyCode\\electron-quicstart\\resources\\sp01.mp4");

        String s = Mp4ParserUtils.mergeVideo(videoList, file);
        System.out.println(s);
    }
}
